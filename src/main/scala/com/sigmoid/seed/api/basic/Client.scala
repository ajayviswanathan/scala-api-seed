package com.sigmoid.seed.api.basic

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, Uri}
import akka.stream.ActorMaterializer

/**
  * Created by ajay on 29/07/16.
  */

/**
  * https://github.com/vrcod/akka-sample-http-stream#master
  */
object Client extends App {

  new Thread(new Server).start()

  implicit val system = ActorSystem("client")
  import system.dispatcher
  implicit val materializer = ActorMaterializer()

  val source = Uri("http://localhost:8200/stream")
  val stream = Http().singleRequest(HttpRequest(uri = source)).flatMap { response =>
    response.entity.dataBytes.runForeach { chunk =>
      println(chunk.utf8String)
    }
  }

}
