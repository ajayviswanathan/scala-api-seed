package com.sigmoid.seed.api.basic

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.{Sink, Source}

import scala.concurrent.duration._

/**
  * Created by ajay on 29/07/16.
  */
class Server extends Runnable {

  def run() = {

    implicit val system = ActorSystem("server")
    implicit val materializer = ActorMaterializer()

    val serverSource = Http().bind(interface = "localhost", port = 8200)

    val requestHandler: HttpRequest => HttpResponse = {
      case HttpRequest(HttpMethods.GET, Uri.Path("/stream"), _, _, _) =>
        HttpResponse(entity = HttpEntity.Chunked(ContentTypes.`text/plain(UTF-8)`, Source.tick(0 seconds, 1 seconds, "text")))
    }

    serverSource.to(Sink.foreach { connection =>
      connection.handleWithSyncHandler(requestHandler)
    }).run()
  }

}
