package com.sigmoid.seed.api.logging

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport

/**
  * Created by ajay on 29/07/16.
  */
trait BaseService extends BaseComponent with Protocol with SprayJsonSupport with Config
