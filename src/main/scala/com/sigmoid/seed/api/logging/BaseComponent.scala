package com.sigmoid.seed.api.logging

import akka.event.LoggingAdapter

import scala.concurrent.ExecutionContext

/**
  * Created by ajay on 29/07/16.
  */
trait BaseComponent {

  protected implicit def log: LoggingAdapter
  protected implicit def executor: ExecutionContext

}
