package com.sigmoid.seed.api.logging

import java.lang.management.ManagementFactory
import akka.http.scaladsl.server.Directives._

import scala.concurrent.duration._

/**
  * Created by ajay on 29/07/16.
  */
trait StatusService extends BaseService {

  protected val statusRoutes = pathPrefix("status") {
    get {
      log.info("/status executed")
      complete(Status(Duration(ManagementFactory.getRuntimeMXBean.getUptime, MILLISECONDS).toString))
    }
  }

}
