package com.sigmoid.seed.api.logging

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer

/**
  * Created by ajay on 29/07/16.
  */
object System {

  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()

  trait LoggerExecutor extends BaseComponent {
    protected implicit val executor = system.dispatcher
    protected implicit val log = Logging(system, "app")
  }

}

/**
  * https://github.com/yeghishe/minimal-scala-akka-http-seed
  */
object Main extends App with Config with System.LoggerExecutor with StatusService {
  import System._
  import Directives._

  Http().bindAndHandle(statusRoutes, httpConfig.interface, httpConfig.port)
}