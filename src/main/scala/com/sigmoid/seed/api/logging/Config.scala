package com.sigmoid.seed.api.logging

import com.typesafe.config.ConfigFactory

/**
  * Created by ajay on 29/07/16.
  */
trait Config {

  protected case class HttpConfig(interface: String, port: Int)

  // private val config = ConfigFactory.load()
  protected val httpConfig = HttpConfig("localhost", 8000) //config.as[HttpConfig]("http")

}
