package com.sigmoid.seed.api.logging

import spray.json.DefaultJsonProtocol

/**
  * Created by ajay on 29/07/16.
  */
case class Status(uptime: String)

trait Protocol extends DefaultJsonProtocol {

  implicit val statusFormatter = jsonFormat1(Status.apply)

}
