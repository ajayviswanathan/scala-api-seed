package com.sigmoid.seed.api.spark.factory

import com.typesafe.config.ConfigFactory
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by ajay on 30/07/16.
  */
trait DatabaseAccess {

  val config = ConfigFactory.load("application.conf")

  val sparkConf: SparkConf = new SparkConf().setAppName("spark-akka-http").setMaster("local")
  val sc = new SparkContext(sparkConf)

}

object DatabaseAccess extends DatabaseAccess
