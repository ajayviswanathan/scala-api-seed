package com.sigmoid.seed.api.spark.routes

import java.util.UUID

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.{HttpResponse, StatusCodes}
import akka.http.scaladsl.server.{ExceptionHandler, Route}
import akka.stream.ActorMaterializer
import com.sigmoid.seed.api.spark.factory.DatabaseAccess

/**
  * Created by ajay on 29/07/16.
  */
trait SparkService extends DatabaseAccess {

  implicit val system: ActorSystem
  implicit val materializer: ActorMaterializer
  val logger = Logging(system, getClass)

  implicit def exceptionHandler =
    ExceptionHandler {
      case e: ArithmeticException =>
        extractUri { uri =>
          complete(HttpResponse(StatusCodes.InternalServerError, entity = "Oopsie!"))
        }
    }

  val sparkRoutes: Route = {
    get {
      path("insert" / "name" / Segment / "email" / Segment) { (name: String, email: String) =>
        complete {
          val documentId = UUID.randomUUID().toString
          try {
            logger.info(s"$name, $email")
            HttpResponse(StatusCodes.Created, entity = s"Data is successfully persisted with id $documentId")
          } catch {
            case e: Throwable =>
              logger.error(e, e.getMessage)
              HttpResponse(StatusCodes.InternalServerError, entity = s"Error found for id $documentId")
          }
        }
      }
    }
  }

}