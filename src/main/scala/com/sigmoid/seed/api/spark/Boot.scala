package com.sigmoid.seed.api.spark

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.event.Logging.LogLevel
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteResult.Complete
import akka.http.scaladsl.server.directives.{DebuggingDirectives, LogEntry, LoggingMagnet}
import akka.stream.{ActorMaterializer, Materializer}
import com.sigmoid.seed.api.spark.routes.SparkService
import pl.project13.scala.rainbow.Rainbow._

import scala.concurrent.ExecutionContext

/**
  * Created by ajay on 29/07/16.
  */
class StartSparkServer(implicit val system: ActorSystem, implicit val materializer: ActorMaterializer) extends SparkService {

  import system.dispatcher

  def startServer(address: String, port: Int) = {
    val loggedRoute = requestMethodAndResponseStatusAsInfo(Logging.InfoLevel, sparkRoutes)
    Http().bindAndHandle(loggedRoute, address, port)
  }

  def requestMethodAndResponseStatusAsInfo(level: LogLevel, route: Route)(implicit m: Materializer, ex: ExecutionContext) = {
    def akkaResponseTimeLoggingFunction(loggingAdapter: LoggingAdapter, requestTimestamp: Long)(req: HttpRequest)(res: Any): Unit = {
      val entry = res match {
        case Complete(resp) =>
          val responseTimestamp: Long = System.currentTimeMillis()
          val elapsedTime: Long = responseTimestamp - requestTimestamp
          val loggingString = s"Logged Request:${req.method}:${req.uri}:${resp.status}:$elapsedTime"
          val colouredLoggingString = if (elapsedTime > StartApp.threshold) {
            loggingString.red
          } else {
            loggingString.green
          }
          LogEntry(colouredLoggingString, level)
        case anythingElse =>
          LogEntry(s"$anythingElse", level)
      }
      entry.logTo(loggingAdapter)
    }
    DebuggingDirectives.logRequestResult(LoggingMagnet(log => {
      val requestTimestamp = System.currentTimeMillis()
      akkaResponseTimeLoggingFunction(log, requestTimestamp)
    }))(route)
  }

}

object StartApplication extends App {
  StartApp
}

object StartApp {

  implicit val system: ActorSystem = ActorSystem("spark-service")
  implicit val executor = system.dispatcher
  implicit val materializer = ActorMaterializer()

  val server = new StartSparkServer()
  val config = server.config
  val interface = config.getString("http.interface")
  val port = config.getInt("http.port")
  val threshold = config.getInt("http.threshold")

  server.startServer(interface, port)

}