package com.sigmoid.seed.api.stream.domain

import akka.http.scaladsl.model.StatusCodes
import com.sigmoid.seed.api.stream.controllers.AppExceptionWithStatusCode

import scala.collection.mutable.{Map => MutableMap}
import scala.concurrent.{ExecutionContext, Future}

/**
  * Created by ajay on 30/07/16.
  */
case class Employee(id: String, name: String, designation: String)

object EmployeeService {

  val employees: MutableMap[String, Employee] = MutableMap(
    "SV-041" -> ("Ajay Viswanathan", "Sr. Software Engineer")
  ).map { e => e._1 -> Employee(e._1, e._2._1, e._2._2)}

  def getAllEmployees: List[Employee] = this.employees.values.toList

  def update(id: String, employee: Employee)(implicit ec: ExecutionContext): Future[Employee] = {
    if (id != employee.id)
      Future.failed(new AppExceptionWithStatusCode(StatusCodes.BadRequest))
    else if (employees.isDefinedAt(employee.id))
      Future.successful {
        employees += employee.id -> employee
        employee
      }
    else
      Future.failed(new AppExceptionWithStatusCode(StatusCodes.NotFound))
  }

  def add(employee: Employee)(implicit ec: ExecutionContext): Future[Employee] = {
    if (employee.id == null)
      Future.failed(new AppExceptionWithStatusCode(StatusCodes.BadRequest))
    else if (employees.exists(e => e._2.name == employee.name))
      Future.failed(new AppExceptionWithStatusCode(StatusCodes.Conflict))
    else
      Future {
        employees += employee.id -> employee
        employee
      }
  }

  def remove(id: String)(implicit ec: ExecutionContext): Future[String] = {
    Future {
      employees -= id
      id
    }
  }

}
