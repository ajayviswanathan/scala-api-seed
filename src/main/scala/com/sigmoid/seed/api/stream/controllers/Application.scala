package com.sigmoid.seed.api.stream.controllers

import akka.NotUsed
import akka.actor.{Actor, ActorLogging}
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.{ExceptionHandler, Route, RouteResult}
import akka.http.scaladsl.settings.{ParserSettings, RoutingSettings}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.LogEntry
import akka.stream.ActorMaterializer
import akka.stream.scaladsl.Flow
import com.sigmoid.seed.api.stream.domain.{Employee, EmployeeService}
import spray.json.{DefaultJsonProtocol, JsonFormat}

import scala.concurrent.Future

/**
  * Created by ajay on 30/07/16.
  */
object Main {

  def main(args: Array[String]): Unit = {
    akka.Main.main(Array(classOf[Application].getName))
  }

}

// To protect against JSON hijacking for older browsers
// Always return JSON with an Object on the outside
case class ArrayWrapper[T](wrappedArray: T)

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val employeeFormat = jsonFormat3(Employee)
  implicit def arrayWrapper[T: JsonFormat] = jsonFormat1(ArrayWrapper.apply[T])
}

class Application extends Actor with ActorLogging with JsonSupport {

  private val interface = "localhost"
  private val port = 8000

  private var fServerBinding: Future[Http.ServerBinding] = _

  override def preStart = {

    implicit val actorSystem = this.context.system
    import actorSystem.dispatcher
    implicit val materializer = ActorMaterializer.create(actorSystem)
    implicit val rs = RoutingSettings(actorSystem)
    implicit val ps = ParserSettings(actorSystem)

    val employeeRoute: Route =
      pathPrefix("app" / "employees") {
        handleExceptions(exceptionHandler) {
          get {
            complete(ArrayWrapper(EmployeeService.getAllEmployees))
          } ~
            put {
              path(Segment) { id =>
                entity(as[Employee]) { employee =>
                  onSuccess(EmployeeService.update(id, employee)) { _ =>
                    complete("updated")
                  }
                }
              }
            } ~
            post {
              pathEnd {
                entity(as[Employee]) { employee =>
                  onSuccess(EmployeeService.add(employee)) { _ =>
                    complete("added")
                  }
                }
              }
            } ~
            delete {
              path(Segment) { id =>
                onSuccess(EmployeeService.remove(id)) { _ =>
                  complete("deleted")
                }
              }
            } ~
            complete(StatusCodes.MethodNotAllowed)
        }
      }

    val shutdownRoute: Route =
      path("shutdown") {
        self ! "shutdown"
        complete(HttpEntity("Shutting down..."))
      }

    def logAccess(innerRoute: Route): Route = {
      def toLogEntry(marker: String, f: Any => String) = (r: Any) => LogEntry(marker + f(r), akka.event.Logging.InfoLevel)
      extractRequest { request =>
        logResult(toLogEntry(s"${request.method.name} ${request.uri} ===> ", {
          case c: RouteResult.Complete => c.response.status.toString
          case x => s"unknown response part of type ${x.getClass}"
        }))(innerRoute)
      }
    }

    val compoundRoute: Route =
      logAccess {
        shutdownRoute ~
          encodeResponse {
            employeeRoute
          }
      }

    val routeFlow: Flow[HttpRequest, HttpResponse, NotUsed] = Route.handlerFlow(compoundRoute)
    fServerBinding = Http().bindAndHandle(routeFlow, interface = interface, port = port)
    log.info(s"listening to http://$interface:$port")
  }

  override def postStop = {
    fServerBinding.onSuccess {
      case sb => sb.unbind()
    }(context.dispatcher)
  }

  override def receive: Receive = {
    case "shutdown" => context.stop(self)
  }

  val exceptionHandler = ExceptionHandler {
    case s: SpecifiesErrorCode =>
      extractUri { uri =>
        log.warning("Request to {} could not be handled normally", uri)
        complete(HttpResponse(s.code))
      }
  }

}

trait SpecifiesErrorCode {
  val code: StatusCode
}

class AppExceptionWithStatusCode(val code: StatusCode) extends Exception with SpecifiesErrorCode