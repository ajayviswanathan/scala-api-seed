package com.sigmoid.seed.api.swagger.hello

import akka.actor.{Actor, ActorLogging}
import io.swagger.annotations.{ApiModel, ApiModelProperty}

import scala.annotation.meta.field

/**
  * Created by ajay on 03/08/16.
  */
object HelloActor {
  @ApiModel(description = "Generic message")
  case object AnonymousHello

  @ApiModel(description = "Personalized message")
  case class Hello(
                    @(ApiModelProperty @field)(value = "Name of the user")
                    name: String)

  @ApiModel(description = "Generic greeting")
  case class Greeting(
                       @(ApiModelProperty @field)(value = "The greeting message")
                       greeting: String)
}

class HelloActor extends Actor with ActorLogging {
  import HelloActor._

  def receive: Receive = {
    case AnonymousHello =>
      sender ! Greeting("Hello")
    case Hello(name) =>
      sender ! Greeting(s"Hello, $name")
  }
}
