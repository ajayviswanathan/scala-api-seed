package com.sigmoid.seed.api.swagger

import akka.actor.{ActorSystem, Props}
import com.sigmoid.seed.api.swagger.add.AddActor
import com.sigmoid.seed.api.swagger.hello.HelloActor

/**
  * Created by ajay on 03/08/16.
  */
trait Core {

  implicit def system: ActorSystem

}

trait BootedCore extends Core {

  implicit lazy val system = ActorSystem("akka-swagger")

  sys.addShutdownHook(system.terminate())
}

trait CoreActors {
  this: Core =>

  val add = system.actorOf(Props[AddActor])
  val hello = system.actorOf(Props[HelloActor])
}


