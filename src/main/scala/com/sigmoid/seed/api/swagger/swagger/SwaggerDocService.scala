package com.sigmoid.seed.api.swagger.swagger

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializer
import com.github.swagger.akka.model.Info
import com.github.swagger.akka.{HasActorSystem, SwaggerHttpService}
import com.sigmoid.seed.api.swagger.add.AddService
import com.sigmoid.seed.api.swagger.hello.HelloService
import io.swagger.models.ExternalDocs
import io.swagger.models.auth.BasicAuthDefinition

import scala.reflect.runtime.{universe => ru}

/**
  * Created by ajay on 03/08/16.
  */
class SwaggerDocService(system: ActorSystem) extends SwaggerHttpService with HasActorSystem {

  implicit val actorSystem: ActorSystem = system
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  val apiTypes = Seq(ru.typeOf[AddService], ru.typeOf[HelloService])
  override val host = "localhost:8000"
  override val info = Info(version = "1.0")
  override val basePath = "/api"
  override val apiDocsPath = "docs"
  override val externalDocs = Some(new ExternalDocs("Sigmoid", "http://sigmoid.com"))
  override val securitySchemeDefinitions = Map("basicAuth" -> new BasicAuthDefinition())

}

trait Site extends Directives {
  val site =
    path("docs") { getFromResource("swagger/index.html") } ~
      getFromResourceDirectory("swagger")
}