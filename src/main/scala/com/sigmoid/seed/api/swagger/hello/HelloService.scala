package com.sigmoid.seed.api.swagger.hello

import javax.ws.rs.Path

import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives
import akka.util.Timeout
import com.sigmoid.seed.api.swagger.DefaultJsonFormats
import com.sigmoid.seed.api.swagger.hello.HelloActor.{AnonymousHello, Greeting, Hello}
import io.swagger.annotations._

import scala.concurrent.ExecutionContext

/**
  * Created by ajay on 03/08/16.
  */
@Api(value = "/hello", description = "Hello template", produces = "application/json")
@Path("/hello")
class HelloService(helloActor: ActorRef)(implicit executionContext: ExecutionContext) extends Directives with DefaultJsonFormats {
  import akka.pattern.ask

  import scala.concurrent.duration._

  implicit val timeout = Timeout(2.seconds)

  implicit val greetingFormat = jsonFormat1(Greeting)

  val route =
    getHello ~
    getHelloSegment

  @ApiOperation(value = "Return hello greeting", nickname = "anonymousHello", httpMethod = "GET")
  @ApiResponses(Array(
    new ApiResponse(code = 200, message = "Return hello greeting", response = classOf[Greeting]),
    new ApiResponse(code = 500, message = "Internal server error")
  ))
  def getHello =
    path("hello") {
      get {
        complete {
          (helloActor ? AnonymousHello).mapTo[Greeting]
        }
      }
    }

  @Path("/{name}")
  @ApiOperation(value = "Return personalized hello greeting", nickname = "hello", httpMethod = "GET")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "name", required = true, dataType = "string", paramType = "path", value = "User name for personalized greeting")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 200, message = "Return hello greeting", response = classOf[Greeting]),
    new ApiResponse(code = 500, message = "Internal server error")
  ))
  def getHelloSegment =
    path("hello" / Segment) { name =>
      get {
        complete {
          (helloActor ? Hello(name)).mapTo[Greeting]
        }
      }
    }

}
