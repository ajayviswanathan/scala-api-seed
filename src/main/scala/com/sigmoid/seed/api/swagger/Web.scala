package com.sigmoid.seed.api.swagger

import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer

/**
  * Created by ajay on 03/08/16.
  */
trait Web {
  this: Api with CoreActors with Core =>

  implicit val materializer = ActorMaterializer()

  Http().bindAndHandle(routes, "localhost", 8000)
}
