package com.sigmoid.seed.api.swagger


import javax.ws.rs.Path

import akka.http.scaladsl.server.RouteConcatenation
import com.sigmoid.seed.api.swagger.add.AddService
import com.sigmoid.seed.api.swagger.hello.HelloService
import com.sigmoid.seed.api.swagger.swagger.{Site, SwaggerDocService}
import io.swagger.annotations.{Api => ApiSwag, ApiOperation, _}

/**
  * Created by ajay on 03/08/16.
  */
@ApiSwag(value = "/add", description = "Add numbers", produces = "application/json")
@Path("/api")
trait Api extends RouteConcatenation with CorsSupport with Site {
  this: CoreActors with Core =>

  private implicit val _ = system.dispatcher

  val routes = {
    pathPrefix("api") {
      new AddService(add).route ~
        new HelloService(hello).route

    } ~
      corsHandler(new SwaggerDocService(system).routes) ~
      site
  }

}
