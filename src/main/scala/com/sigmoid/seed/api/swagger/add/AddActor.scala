package com.sigmoid.seed.api.swagger.add

import akka.actor.{Actor, ActorLogging}
import io.swagger.annotations.{ApiModel, ApiModelProperty}

import scala.annotation.meta.field

/**
  * Created by ajay on 03/08/16.
  */
object AddActor {
  @ApiModel(description = "An add request")
  case class AddRequest(
                         @(ApiModelProperty @field)(value = "An array of integers")
                         numbers: Array[Int]
                       )
  @ApiModel(description = "An add response")
  case class AddResponse(
                          @(ApiModelProperty @field)(value = "The resultant sum")
                          sum: Int
                        )
}

class AddActor extends Actor with ActorLogging {
  import AddActor._

  def receive: Receive = {
    case AddRequest(numbers) =>
      sender ! AddResponse(numbers.sum)
  }
}
