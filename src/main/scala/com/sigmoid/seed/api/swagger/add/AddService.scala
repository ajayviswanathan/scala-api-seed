package com.sigmoid.seed.api.swagger.add

import javax.ws.rs.Path

import akka.actor.ActorRef
import akka.http.scaladsl.server.Directives
import akka.util.Timeout
import com.sigmoid.seed.api.swagger.DefaultJsonFormats
import com.sigmoid.seed.api.swagger.add.AddActor.{AddRequest, AddResponse}
import io.swagger.annotations._

import scala.concurrent.ExecutionContext

/**
  * Created by ajay on 03/08/16.
  */
@Api(value = "/add", description = "Add numbers", produces = "application/json")
@Path("/add")
class AddService(addActor: ActorRef)(implicit executionContext: ExecutionContext) extends Directives with DefaultJsonFormats {
  import akka.pattern.ask

  import scala.concurrent.duration._

  implicit val timeout = Timeout(2.seconds)

  implicit val requestFormat = jsonFormat1(AddRequest)
  implicit val responseFormat = jsonFormat1(AddResponse)

  val route = add

  @ApiOperation(value = "Add integers", nickname = "addIntegers", httpMethod = "POST")
  @ApiImplicitParams(Array(
    new ApiImplicitParam(name = "body", value = "\"numbers\" to sum", required = true, dataType = "com.sigmoid.seed.api.swagger.add.AddActor$AddRequest", paramType = "body")
  ))
  @ApiResponses(Array(
    new ApiResponse(code = 200, message = "Return sum", response = classOf[AddResponse]),
    new ApiResponse(code = 500, message = "Internal server error")
  ))
  def add =
    path("add") {
      post {
        entity(as[AddRequest]) { request =>
          complete {
            (addActor ? request).mapTo[AddResponse]
          }
        }
      }
    }

}
