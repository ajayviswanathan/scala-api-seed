# Scala API Seed
### A collection of REST API design patterns in akka-http

## Basic
Starts two processes, a server and a client and the client repeatedly sends a post request and keep logging

## Logging
Responds to /status with uptime
Has advanced logging features

## Spark
Demonstrating spark integration

## Stream
Demonstrating creation of flows in akka-http

## Swagger
Starter template for integrating swagger docs support for akka-http
`/docs` will open Swagger UI