import com.typesafe.sbt.SbtStartScript

name := "scala-api-seed"

version := "1.0"

scalaVersion := "2.11.8"

val akkaVersion = "2.4.8"
val sparkVersion = "1.6.0"
val scalaLoggingVersion = "3.4.0"
val logbackVersion = "1.1.7"
val scalaRainbowVersion = "0.2"
val scalaGuiceVersion = "4.0.1"
val swaggerAkkaHttpVersion = "0.7.2"
val scalaTestVersion = "2.2.6"
val scalaMockVersion = "3.2.2"

// Akka Dependencies
val akkaHttpCore = "com.typesafe.akka" %% "akka-http-core" % akkaVersion
val akkaStream = "com.typesafe.akka" %% "akka-stream" % akkaVersion
val akkaHttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaVersion

// Spark Dependencies
val sparkCore = "org.apache.spark" %% "spark-core" % sparkVersion

// Logging Dependencies
val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion
val logback = "ch.qos.logback" %  "logback-classic" % logbackVersion
val scalaRainbow = "pl.project13.scala" %% "rainbow" % scalaRainbowVersion

// Injection Dependencies
val scalaGuice = "net.codingwell" %% "scala-guice" % scalaGuiceVersion

// Documentation Dependencies
val swaggerAkkaHttp = "com.github.swagger-akka-http" %% "swagger-akka-http" % swaggerAkkaHttpVersion

// Test Dependencies
val scalatest = "org.scalatest" %% "scalatest" % scalaTestVersion % "test"
val scalamock = "org.scalamock" %% "scalamock-scalatest-support" % scalaMockVersion % "test"
val akkaHttpTestkit = "com.typesafe.akka" %% "akka-http-testkit" % akkaVersion % "test"
val akkaStreamTestkit = "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % "test"

libraryDependencies ++= {
  Seq(
    akkaHttpCore
    , akkaHttpSprayJson
    , sparkCore
    , scalaLogging
    , logback
    , scalaRainbow
    , swaggerAkkaHttp
    , scalatest
    , scalamock
    , akkaHttpTestkit
  )
}

// fixes: SBT project import: [warn] Multiple depenedencies with the same organization/name but different versions
ivyScala := ivyScala.value map {
  _.copy(overrideScalaVersion = true)
}

// To use separate JVM from sbt when running
fork in run := true

// sbt-script settings
seq(SbtStartScript.startScriptForClassesSettings: _*)